const User = require('../ models/user.model');

exports.mapUser = (req) => new User({
  email: req.body.email,
  password: req.body.password,
  nombre: req.body.nombre,
  apellido: req.body.apellido,
  telefono: req.body.telefono,
  fechaNacimiento: req.body.fechaNacimiento,
  colaboraciones: [],
  impulsadas: [],
});
