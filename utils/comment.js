const Comment = require('../ models/comment.model');

exports.mapComment = (req) => {
  const date = new Date();
  const year = date.getFullYear();
  const month = (1 + date.getMonth()).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  const newdate = month + '/' + day + '/' + year;
  return Comment({
    email: req.body.mail,
    idIniciativa: req.body.idIniciativa,
    comentario: req.body.comment,
    fecha: newdate,
  });
};

