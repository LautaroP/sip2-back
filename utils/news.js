const News = require('../ models/news.model');

exports.mapNews = (req) => new News({
  title: req.body.title,
  body: req.body.body,
  photo: req.body.photo,
  date: new Date(),
});
