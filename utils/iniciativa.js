const Iniciativa = require('../ models/iniciativa.model');

exports.mapIniciativaLimite = (req) => {
  const date = req.body.fechaLimite;
  const datearray = date.split('/');
  const newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
  return Iniciativa(
      {
        titulo: req.body.titulo,
        geoLocalizacion: req.body.geoLocalizacion,
        fechaCreacion: new Date(),
        fechaLimite: newdate,
        categoria: req.body.categoria,
        descripcion: req.body.descripcion,
        impulsor: req.body.impulsor,
        objetivoPersonaEsperado: req.body.objetivoPersonaEsperado,
        objetivoPersonaAcumulado: 0,
        objetivoEconomicoEsperado: req.body.objetivoEconomicoEsperado,
        objetivoEconomicoAcumulado: 0,
        activo: true,
        colaboradores: [],
      },
  );
};

exports.mapIniciativa = (req) => {
  return Iniciativa(
      {
        titulo: req.body.titulo,
        geoLocalizacion: req.body.geoLocalizacion,
        categoria: req.body.categoria,
        descripcion: req.body.descripcion,
        fechaCreacion: new Date(),
        impulsor: req.body.impulsor,
        objetivoPersonaEsperado: req.body.objetivoPersonaEsperado,
        objetivoPersonaAcumulado: 0,
        objetivoEconomicoEsperado: req.body.objetivoEconomicoEsperado,
        objetivoEconomicoAcumulado: 0,
        activo: true,
        colaboradores: [],
      },
  );
};

