const Donacion = require('../ models/donacion.model');

exports.mapDonacion = (idIniciativa, mail, donacion) => {
  const date = new Date();
  const year = date.getFullYear();
  const month = (1 + date.getMonth()).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  const newdate = month + '/' + day + '/' + year;
  return Donacion(
      {
        idIniciativa: idIniciativa,
        email: mail,
        monto: donacion,
        fecha: newdate,
      },
  );
};
