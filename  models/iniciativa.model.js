var connection = require('../mongooseConnection');

var iniciativaSchema = connection.Schema({
  titulo: { type: String, required: true, max: 100 },
  geoLocalizacion: { type: String, required: false },
  fechaLimite: { type: Date, required: false },
  categoria: { type: String, required: true },
  descripcion: { type: String, required: false },
  impulsor: { type: String, required: true },
  objetivoPersonaEsperado: { type: Number, required: false },
  objetivoPersonaAcumulado: { type: Number, required: false },
  objetivoEconomicoEsperado: { type: Number, required: false },
  objetivoEconomicoAcumulado: { type: Number, required: false },
  activo: { type: Boolean, required: true },
  colaboradores: { type: Array, required: true },
  fechaCreacion : { type: Date, required: true },
});

module.exports = connection.model('Iniciativa', iniciativaSchema);
