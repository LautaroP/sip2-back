var connection = require('../mongooseConnection');

var newsSchema = connection.Schema({
    title: { type: String, required: true },
    body: { type: String, required: true },
    photo: { type: String, required: false },
    date: {type: Date, required:true}
});

module.exports = connection.model('Noticia', newsSchema);