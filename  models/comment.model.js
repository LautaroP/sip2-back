const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = Schema({
  email: { type: String, required: true },
  idIniciativa: { type: String, required: true },
  comentario: { type: String, required: true },
  fecha: {type: Date, required: true},
});

module.exports = mongoose.model('Comentario', CommentSchema);