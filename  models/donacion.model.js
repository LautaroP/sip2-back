var connection = require('../mongooseConnection');

var donacionSchema = connection.Schema({
    idIniciativa: { type: String, required: true },
    email: { type: String, required: true },
    monto: { type: Number, required: true },
    fecha: { type: Date, required: true },
});

module.exports = connection.model('Donacion', donacionSchema);