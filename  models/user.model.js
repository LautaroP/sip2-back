const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = Schema({
  email: {type: String, required: true},
  password: {type: String, required: true},
  nombre: {type: String, required: true},
  apellido: {type: String, required: true},
  fechaNacimiento: {type: Date, required: false},
  telefono: {type: String, required: true},
  colaboraciones: {type: Array, required: true},
  impulsadas: {type: Array, required: true},
});

module.exports = mongoose.model('Usuario', UserSchema);
