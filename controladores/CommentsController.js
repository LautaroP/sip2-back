const CommentService = require('../service/comment.service');
const UsuariosService = require('../service/user.service');
const {mapComment} = require('../utils/comment');
const IniciativaService = require('../service/iniciativa.service');

exports.getCommentsByIniciativa = (req, res) => {
  const idIniciativa = req.params.id;
  CommentService.getCommentsByIdIniciativa(idIniciativa)
      .then((result) => {
        if (result != null) {
          if (result.length > 0) {
            return res.json(result);
          }
          return res.status(200).send([]);
        }
        return res.status(500).send('Internal server error');
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Internal server error');
      });
};

exports.getCommentsByMail = (req, res) => {
  const mail = req.params.mail;
  UsuariosService.isUser(mail)
      .then((existeUsuario) => {
        if (existeUsuario) {
          CommentService.getCommentsByMailUsuario(mail)
              .then((result) => {
                if (result != null) {
                  if (result.length > 0) {
                    return res.json(result);
                  }
                  return res.status(200).send([]);
                }
                return res.status(500).send('Internal server error');
              });
        } else {
          return res.status(404).send('No existe usuario con ese mail');
        };
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Internal server error');
      });
};

exports.insertComment = (req, res) => {
  const newComment = mapComment(req);
  UsuariosService.isUser(req.body.mail).then((existeUsuario) => {
    if (existeUsuario) {
      IniciativaService.getEventById(req.body.idIniciativa)
          .then((result) =>{
            if (result != null) {
              console.log(result);
              CommentService.insertComment(newComment)
                  .then((result) => {
                    if (result != null) {
                      return res.json(result);
                    }
                    return res.status(500).send('Internal server error when saving comment');
                  });
            } else {
              return res.status(404).send('No existe iniciativa con ese id');
            }
          });
    } else {
      return res.status(404).send('No existe usuario con ese mail');
    }
  })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Internal server error when saving comment');
      });
};
