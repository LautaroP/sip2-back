const {mapDonacion} = require('../utils/donacion');
const DonacionService = require('../service/donacion.service');

exports.insertDonacion = (idIniciativa, mail, donacion) => {
  const newDonacion = mapDonacion(idIniciativa, mail, donacion);
  return DonacionService
      .createDonacion(newDonacion)
      .then((nuevaDonacion) => {
        if (nuevaDonacion) {
          return result;
        }
        return null;
      }).catch((error) => error);
};

