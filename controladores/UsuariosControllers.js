const {mapUser} = require('../utils/usuario');
const UserService = require('../service/user.service');
const IniciativService = require('../service/iniciativa.service');

exports.insertUsuario = (req, res) => {
  const email = req.body.email;
  UserService.getUserById(email)
      .then((result) => {
        console.log(result);
        if (result == null) {
          const newUser = mapUser(req);
          return UserService.createUser(newUser)
              .then((result2) => {
                if (result2) {
                  return res.status(200).send('User created!');
                }
                return res.status(500).send('Server error');
              });
        }
        return res.status(400).send('User already exists');
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Server error');
      });
};

exports.getUsuarios = (res) => UserService.getUsers()
    .then((result) => {
      if (res) {
        if (res.length > 0) {
          return res.json(result);
        }
        return res.status(204).send('');
      }
      return res.statos(500).send('Internal server error');
    }).catch((err) => {
      console.log(err);
      return res.statos(500).send('Internal server error');
    });

exports.login = (req, res) => UserService.validateUser(req)
    .then((result) => {
      if (result == null) {
        return res.status(404).send('Usuario no encontrado');
      }
      if (result) {
        if (result.password == req.body.password) {
          return res.json(result);
        }
        return res.status(401).send('Usuario invalido');
      }
    }).catch((err) => {
      console.log(err);
      return res.statos(500).send('Internal server error');
    }); ;


exports.getUsuarioByMail = (req, res) => {
  const email = req.body.email;
  UserService.getUserById(email)
      .then(
          (user) => {
            console.log(user);
            if (user != null) {
              return res.json(user);
            }
            return res.status(404).send('User not found');
          },
      )
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Internal Server error');
      });
};

exports.setPassword = (req, res) => UserService.updatePassword(req.body)
    .then((result) => {
      if (result) {
        return res.status(200).send('Password updated');
      }
      return res.status(500).send('Internal Server error');
    }).catch((err) => {
      console.log(err);
      res.status(500).send('Internal Server error');
    });

exports.showColaboraciones = async (req, res) => {
  let idColaboraciones;
  try {
    idColaboraciones = await UserService.getColaboraciones(req.body.mail);
    idColaboraciones = idColaboraciones[0].colaboraciones;
  } catch (err) {
    return res.status(404).json('No existe usuario con ese mail');
  }
  try {
    const colaboraciones = await IniciativService.getColaboracionesByIds(idColaboraciones);
    return res.json(colaboraciones);
  } catch (err) {
    console.log(err);
    return res.status(500).send('Internal Server error');
  }
};
