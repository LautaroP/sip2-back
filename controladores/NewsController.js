const {mapNews} = require('../utils/news');
const NewsService = require('../service/news.service');

exports.insertNews = (req, res) => {
    const news = mapNews(req);
        return NewsService.insertNews(news)
            .then((result2) => {
                if (result2) {
                    return res.status(200).send('News created!');
                }
            return res.status(500).send('Server error');
        })
        .catch((err) => {
          console.log(err);
          return res.status(500).send('Server error');
        });
};

exports.getNews = (req, res) => {
        return NewsService.getNews()
            .then((result2) => {
                if (result2) {
                    return res.json(result2);
                }
            return res.status(500).send('Server error');
        })
        .catch((err) => {
          console.log(err);
          return res.status(500).send('Server error');
        });
};