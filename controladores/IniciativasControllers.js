const {mapIniciativaLimite, mapIniciativa} = require('../utils/iniciativa');
const IniciativaService = require('../service/iniciativa.service');
const UsuariosService = require('../service/user.service');
const DonacionController = require('../controladores/DonacionesControllers');

exports.insertIniciativa = (req, res) => {
  const newIniciativa = (req.body.fechaLimite == null || req.body.fechaLimite == undefined) ?
  mapIniciativa(req) :
  mapIniciativaLimite(req);
  IniciativaService
      .createEvent(newIniciativa)
      .then((nuevaIniciativa) => {
        if (nuevaIniciativa) {
          return UsuariosService
              .insertImpulsadasAUsuario(req.body.impulsor, newIniciativa.id)
              .then((result) => {
                console.log(result);
                if (result) {
                  return res.send('Iniciativa guardada');
                }
                return res.status(500).send('Error al guardar iniciativa1 -- Usuario');
              });
        }
        return res.status(500).send('Error al guardar iniciativa2 -- Evento');
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Error al guardar iniciativa3');
      });
};

exports.getIniciativasByCategoria = (req, res) => {
  const category = req.body.categoria;
  IniciativaService.getEventsByCategory(category)
      .then((listaIniciativas) => {
        if (listaIniciativas) {
          if (listaIniciativas.length > 0) {
            return res.json(listaIniciativas);
          }
          return res.json([]);
        }
        return res.status(500).send('Internal server error');
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Internal server error');
      });
};

exports.getEventById = (req, res) => {
  const id = req.body.id;
  IniciativaService.getEventById(id)
      .then(
          (listaIniciativas) => {
            if (listaIniciativas) {
              if (listaIniciativas.length > 0) {
                return res.json(listaIniciativas);
              }
              return res.status(204).send('');
            }
            return res.status(500).send('Internal server error');
          })
      .catch((err) => {
        console.log(err);
        return res.status(500).send('Internal server error');
      });
};

exports.inscribirseEnActividad = (req, res) => {
  const idBusqueda = {_id: req.body.idIniciativa};
  IniciativaService.getEventById(req.body.idIniciativa).then((iniciativa) => {
    if (iniciativa != null) {
      UsuariosService.isUser(req.body.mail).then((existeUsuario) => {
        if (existeUsuario) {
          const isPresent = IniciativaService.isUserPresent(iniciativa, req.body.mail);
          if (!isPresent) {
            let suma = iniciativa.objetivoPersonaAcumulado;
            suma++;
            IniciativaService.updateInscripciones(idBusqueda, req.body.mail, suma)
                .then((iniciativaActualizada) => {
                  if (iniciativaActualizada) {
                    UsuariosService.insertColaboracionAUsuario(req.body.mail, req.body.idIniciativa)
                        .then((usuarioActualizado) => {
                          if (usuarioActualizado != null) {
                            UsuariosService.getUserById(iniciativa.impulsor).then((impulsor) => {
                              if (impulsor != null) {
                                const responseData = {
                                  'message': (suma >= iniciativa.objetivoPersonaEsperado) ? 'Inscripcion realizada y Objetivo alcanzado' : 'Inscripcion realizada',
                                  'impulsor': impulsor,
                                };
                                return res.json(responseData);
                              }
                              return res.status(500).json('Error del Servidor');
                            });
                          } else {
                            return res.status(500).json('Error actualizando los datos del usuario');
                          }
                        });
                  } else {
                    return res.status(500).json('Error actualizando los datos de la iniciativa');
                  }
                });
          } else {
            return res.status(401).json('El usuario ya participa en esta iniciativa');
          }
        } else {
          return res.status(404).json('No existe usuario con ese mail');
        }
      });
    } else {
      return res.status(404).json('No existe iniciativa con ese ID');
    }
  }).catch((err) => {
    console.log(err);
    return res.status(500).json('Error del Servidor');
  });
};

exports.realizarDonacion = (req, res) => {
  const idBusqueda = {_id: req.body.idIniciativa};
  IniciativaService.getEventById(req.body.idIniciativa)
      .then((iniciativa) => {
        if (iniciativa != null) {
          UsuariosService.isUser(req.body.mail).then((existeUsuario) => {
            if (existeUsuario) {
              const isPresent = IniciativaService.isUserPresent(iniciativa, req.body.mail);
              const suma = iniciativa.objetivoEconomicoAcumulado + parseInt(req.body.donacion);
              if (!isPresent) {
                IniciativaService.updateDonacionNueva(idBusqueda, req.body.mail, suma)
                    .then((iniciativaActualizada) => {
                      if (iniciativaActualizada) {
                        UsuariosService.insertColaboracionAUsuario(req.body.mail, req.body.idIniciativa)
                            .then((usuarioActualizado) => {
                              if (usuarioActualizado != null) {
                                DonacionController.insertDonacion(req.body.idIniciativa, req.body.mail, req.body.donacion).then((donacion) => {
                                  if (donacion != null) {
                                    if (suma >= iniciativa.objetivoEconomicoEsperado) {
                                      return res.status(200).send('Objetivo cumplido + Donacion realizada con exito');
                                    } else {
                                      return res.status(200).send('Donacion realizada con exito');
                                    }
                                  } else {
                                    return res.status(500).json('Error actualizando los datos de la donacion');
                                  }
                                });
                              } else {
                                return res.status(500).json('Error actualizando los datos del usuario');
                              }
                            });
                      } else {
                        return res.status(500).json('Error actualizando los datos de la iniciativa');
                      }
                    });
              } else {
                IniciativaService.updateDonacionRepetida(idBusqueda, suma)
                    .then((iniciativaActualizada) => {
                      if (iniciativaActualizada) {
                        DonacionController.insertDonacion(req.body.idIniciativa, req.body.mail, req.body.donacion)
                            .then((donacion) => {
                              if (donacion != null) {
                                if (suma >= iniciativa.objetivoEconomicoEsperado) {
                                  return res.status(200).send('Objetivo cumplido + Donacion realizada con exito');
                                } else {
                                  return res.status(200).send('Donacion realizada con exito');
                                }
                              } else {
                                return res.status(500).json('Error actualizando los datos de la donacion');
                              }
                            });
                      } else {
                        return res.status(500).json('Error actualizando los datos de la iniciativa');
                      }
                    });
              }
            } else {
              return res.status(404).json('No existe usuario con ese mail');
            }
          });
        } else {
          return res.status(404).json('No existe iniciativa con ese ID');
        }
      }).catch((err) => {
        console.log(err);
        return res.status(500).json('Error del Servidor');
      });
};

exports.getAllIniciativas = (req, res) => {
  return IniciativaService.getEvents().then((result) => {
    if (result != null) {
      if (result.length > 0) {
        return res.json(result);
      }
      return res.status(204).send('');
    }
    return res.status(500).send('Error del Servidor');
  }).catch((err) => {
    console.log(err);
    return res.json(err);
  });
};

exports.getIniciativasTopTen = (req, res) => {
  return IniciativaService.getEventsTopTen().then((result) => {
    if (result != null) {
      if (result.length > 0) {
        return res.json(result);
      }
      return res.status(204).send('');
    }
    return res.status(500).send('Error del Servidor');
  }).catch((err) => {
    console.log(err);
    return res.json(err);
  });
};

exports.getIniciativasByWord = (req, res) => {
    const regex2 = new RegExp(req.params.texto,"i");
  return IniciativaService.getEventsByWord(regex2).then((result) => {
    if (result != null) {
      if (result.length > 0) {
        return res.json(result);
      }
      return res.status(404).json('No hay iniciativas');
    }
    return res.status(500).json('Error del Servidor');
  }).catch((err) => {
    console.log(err);
    return res.json(err);
  });
};
