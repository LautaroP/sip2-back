const router = require('express').Router();
const IniciativasController = require('./controladores/IniciativasControllers');
const UsuariosController = require('./controladores/UsuariosControllers');
const DonacionesController = require('./controladores/DonacionesControllers');
const CommentController = require('./controladores/CommentsController');
const NewsController = require('./controladores/NewsController');

const paramUsr = '/:usuario';
const paramPass = '/:pass';
const paramId = '/:id';
const paramMail = '/:mail';

router.get('/', function(req, res) {
  res.json(
      {
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!!!!!!',
      });
});

router.post('/insertIniciativa', function(req, res) {
  IniciativasController.insertIniciativa(req, res);
});

router.post('/getIniciativasByCategoria', function(req, res) {
  IniciativasController.getIniciativasByCategoria(req, res);
});

router.post('/inscribirseEnActividad', function(req, res) {
  IniciativasController.inscribirseEnActividad(req, res);
});

router.post('/realizarDonacion', function(req, res) {
  IniciativasController.realizarDonacion(req, res);
});

router.post('/showColaboraciones', function(req, res) {
  UsuariosController.showColaboraciones(req, res);
});

router.get('/getAllIniciativas', function(req, res) {
  IniciativasController.getAllIniciativas(req, res);
});

router.get('/getCommentsByMail/:mail', function(req, res) {
  CommentController.getCommentsByMail(req, res);
});

router.get('/getCommentsByIniciativa/:id', function(req, res) {
  CommentController.getCommentsByIniciativa(req, res);
});

router.get('/getIniciativasTopTen', function(req, res) {
  IniciativasController.getIniciativasTopTen(req, res);
});

router.post('/insertComentario', function(req, res) {
  CommentController.insertComment(req, res);
});

router.post('/login', function(req, res) {
  UsuariosController.login(req, res);
});

router.post('/insertUsuario', function (req, res) {
  UsuariosController.insertUsuario(req, res);
});

router.post('/getUsuarioByMail', function (req, res) {
  UsuariosController.getUsuarioByMail(req, res);
});

router.get('/getIniciativasByWord/:texto', function (req, res) {
  IniciativasController.getIniciativasByWord(req, res);
});

router.get('/getNews/', function (req, res) {
  NewsController.getNews(req, res);
});

router.post('/insertNews', function(req, res) {
  NewsController.insertNews(req, res);
});

/* SIN USAR
router.post('/getIniciativaById', function (req, res) {
    IniciativasController.getIniciativaById(req, res);
});

router.get('/getUsuarios', function (req, res) {
    UsuariosController.getUsuarios(res);
});

router.post('/login', function (req, res) {
    UsuariosController.login(req, res);
});

router.post('/setPassword', function (req, res) {
    UsuariosController.setPassword(req, res);
});

*/

module.exports = router;
