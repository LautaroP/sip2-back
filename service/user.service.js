/* eslint-disable object-shorthand */
const User = require('../ models/user.model');

exports.getUserById = (email) => User
    .findOne({email: email})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.isUser = (email) => User
    .findOne({email: email}).then((result) => {
      if (result != null) {
        return true;
      } else {
        return false;
      }
    })
    .catch((error) => error);

exports.createUser = (user) => user
    .save()
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => {
      if (error.message.indexOf('Cast to Date failed') !== -1) {
        return null;
      }
      return error;
    });

exports.getUsers = () => User.find({})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.getColaboraciones = (mail) => User
    .find({email: mail})
    .select('colaboraciones -_id')
    .then((result) => {
      if (result.length > 0) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.validateUser = (req) => User
    .findOne({email: req.body.mail})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    },
    ).catch((error) => error);

exports.updatePassword = (requestBody) => User
    .updateOne({email: requestBody.email}, {password: requestBody.password})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.insertImpulsadasAUsuario = (mail, idIniciativa) => {
  const idBusqueda = {email: mail};
  return User
      .findOneAndUpdate(idBusqueda, {$push: {'impulsadas': idIniciativa}}, {new: true})
      .then((doc) => {
        if (doc) {
          return doc;
        }
        return null;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
};

exports.insertColaboracionAUsuario = (mail, idIniciativa) => {
  const idBusqueda = {email: mail};
  return User.findOneAndUpdate(idBusqueda, {$push: {'colaboraciones': idIniciativa}}, {new: true})
      .then((doc) => {
        if (doc) {
          return doc;
        }
        return null;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
};
