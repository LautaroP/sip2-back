/* eslint-disable object-shorthand */
const Iniciativa = require('../ models/iniciativa.model');

exports.getEvents = () => Iniciativa
    .find({activo: true, $or: [ {fechaLimite : {$gte : new Date()}}, {fechaLimite: null } ] } )
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.getEventsTopTen = () => Iniciativa
.find({activo: true, $or: [ {fechaLimite : {$gte : new Date()}}, {fechaLimite: null } ]}).sort({fechaCreacion: -1}).limit(10)
.then((result) => {
  if (result) {
    return result;
  }
  return null;
}).catch((error) => error);

exports.getEventById = (id) => Iniciativa
    .findOne({_id: id})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => {
      if (error.message.indexOf('Cast to ObjectId failed') !== -1) {
        return null;
      }
      return error;
    });

exports.getEventsByWord = (word) => Iniciativa
    .find({titulo: { $regex : word} })
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.createEvent = (event) => event
    .save()
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.getEventsByCategory = (category) => Iniciativa
    .find({categoria: category, activo: true, $or: [ {fechaLimite : {$gte : new Date()}}, {fechaLimite: null } ]})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.updateInscripciones = (idBusqueda, mail, suma) => Iniciativa
    .findOneAndUpdate(idBusqueda, {$set: {objetivoPersonaAcumulado: suma}, $push: {'colaboradores': mail}}, {new: true})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.updateDonacionNueva = (idBusqueda, mail, suma) => Iniciativa
    .findOneAndUpdate(idBusqueda, {$set: {objetivoEconomicoAcumulado: suma}, $push: {'colaboradores': mail}}, {new: true})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.updateDonacionRepetida = (idBusqueda, suma) => Iniciativa
    .findOneAndUpdate(idBusqueda, {$set: {objetivoEconomicoAcumulado: suma}}, {new: true})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.isUserPresent = (iniciativa, email) => {
  const colaboradores = iniciativa.colaboradores;
  if (!colaboradores.includes(email)) {
    return false;
  }
  return true;
};

exports.getColaboracionesByIds = async (idColaboraciones) => {
  try {
    const colaboraciones = Promise.all(idColaboraciones.map((id) => {
      return this.getEventById(id);
    }));
    console.log(colaboraciones);
    return colaboraciones;
  } catch (err) {
    return err;
  }
};

