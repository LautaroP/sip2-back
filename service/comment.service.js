const Comment = require('../ models/comment.model');

exports.getCommentsByIdIniciativa = (idIniciativa) =>
  Comment.find({idIniciativa: idIniciativa})
      .then((result) => {
        if (result) {
          return result;
        }
        return null;
      })
      .catch((err) => {
        return err;
      });

exports.getCommentsByMailUsuario = (mail) =>
  Comment.find({email: mail})
      .then((result) => {
        if (result) {
          return result;
        }
        return null;
      })
      .catch((err) => {
        return err;
      });

exports.insertComment = (comment) =>
  comment.save()
      .then((result) => {
        if (result) {
          return result;
        }
        return null;
      }).catch((error) => error);

