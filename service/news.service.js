const News = require('../ models/news.model');

exports.getNews = () => News
    .find({})
    .then((result) => {
      if (result) {
        return result;
      }
      return null;
    }).catch((error) => error);

exports.insertNews = (news) =>
  news.save()
      .then((result) => {
        if (result) {
          return result;
        }
        return null;
      }).catch((error) => error);
