const Donacion = require('../ models/donacion.model');

exports.createDonacion = (donacion) =>
  donacion
      .save()
      .then((result) => {
        if (result) {
          return result;
        }
        return null;
      }).catch((error) => error);
